'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  // 获取 controller.user1.index   user1要改成后台数据库的名字
  // 登录
  router.get('/login', controller.login.index);//查找
  // 品类管理
  router.get('/classguanli', controller.classguanli.index);//查找
  router.put('/classguanli/put/:id', controller.classguanli.update);
  router.delete('/classguanli/del/:id', controller.classguanli.destroy);
  router.post('/classguanli/add', controller.classguanli.create);
  // 图片上传
  router.post('/upload', controller.lmhUpload.updatepuSubPic);
  // 商品管理
  router.get('/commodity', controller.commodity.index);//查找
  router.put('/commodity/put/:id', controller.commodity.update);
  router.delete('/commodity/del/:id', controller.commodity.destroy);
  router.post('/commodity/add', controller.commodity.create);
  // 商城管理
  router.get('/shopping', controller.shopping.index);//查找
  router.put('/shopping/put/:id', controller.shopping.update);
  router.delete('/shopping/del/:id', controller.shopping.destroy);
  router.post('/shopping/add', controller.shopping.create);
   // 订单管理
  router.get('/order', controller.order.index);//查找
  router.put('/order/put/:id', controller.order.update);
  router.delete('/order/del/:id', controller.order.destroy);
  router.post('/order/add', controller.order.create);
  

  router.get('/jfhq', controller.lgjfhq.index);// 查找
  router.get('/lgjfbg', controller.lgjfbg.index);// 查找
  router.get('/lgjfph', controller.lgjfph.index);// 查找
  router.get('/lgjfmx', controller.lgjfmx.index);// 查找

  router.get('/login', controller.login.index);//查找
  router.get('/activelist', controller.activelist.index);//查找
  router.delete('/activelist/:id', controller.activelist.destroy);//删除
  router.post('/activelist', controller.activelist.create)//增加b b
  router.put('/activelist/:id', controller.activelist.update); // 更新
};
