'use strict';

const Controller = require('egg').Controller;

class user1 extends Controller {
  async index() {
    const data = await this.ctx.service.user1.findAll(this.ctx);
    this.ctx.body = data;
  }
  async create() {
    const ctx = this.ctx;
    const params = ctx.request.body;
    const data = await ctx.service.user1.create(params);
    ctx.body = data;
  }
  async destroy() {
    const ctx = this.ctx;
    const userId = ctx.params.id;
    const del = await ctx.service.user1.delete(userId);
    ctx.body = del;
  }
  async update() {
    const ctx = this.ctx;
    const userId = ctx.params.id;
    const params = ctx.request.body;
    const change = await ctx.service.user1.update(userId, params);
    ctx.body = change;
  }
  async show() {
    const ctx = this.ctx;
    const id = ctx.params.id;
    const user = await this.ctx.service.user1.find(id);
    ctx.body = user;
  }
  async index0() {
    const ctx = this.ctx;
    const pass = ctx.params.age;
    console.log(pass);
    const data = await this.ctx.service.user1.findAll0(pass);
    this.ctx.body = data;
  }
}
module.exports = user1;
