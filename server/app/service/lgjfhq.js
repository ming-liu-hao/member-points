'use strict';

const Service = require('egg').Service;

class lgjfhq extends Service { // lgjfhq改成跟后台数据库的名字一致
  async findAll() {
    const result = await this.app.mysql.select('lgjfhq');
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }// timesocietyitem
  async find(uid) {
    const result = await this.app.mysql.get('lgjfhq', { id: uid });
    if (result !== null) {
      return {
        code: 200,
        msg: '查找成功',
        data: result,
      };
    }
    return {
      msg: '没有此条数据！',
    };

  }
  // 往后台传动系
  async create(params) {
    const cfg = {
      id: params.id,
      name: params.name,
      age: params.age,
      img: params.img,
    };
    const result = await this.app.mysql.insert('lgjfhq', cfg);
    return {
      code: 200,
      msg: '创建成功',
      data: result,
    };
  }
  async delete(uid) {
    const result = await this.app.mysql.delete('lgjfhq', { id: uid });
    return {
      code: 200,
      msg: '删除成功',
      data: result,
    };
  }
  async update(uid, params) {
    const cfg = {
      id: uid,
      name: params.name,
      age: params.age,
      img: params.img,
    };
    const result = await this.app.mysql.update('lgjfhq', cfg);
    return {
      code: 200,
      msg: '更新成功',
      data: result,
    };
  }
  async findAll0(pass) {
    const result = await this.app.mysql.select('lgjfhq', {
      where: { pass },
    });
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }
}

module.exports = lgjfhq;
