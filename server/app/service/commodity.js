'use strict';

const Service = require('egg').Service;

class commodity extends Service { // commodity改成跟后台数据库的名字一致
  async findAll() {
    const result = await this.app.mysql.select('commodity');
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }// timesocietyitem
  async find(uid) {
    const result = await this.app.mysql.get('commodity', { id: uid });
    if (result !== null) {
      return {
        code: 200,
        msg: '查找成功',
        data: result,
      };
    }
    return {
      msg: '没有此条数据！',
    };

  }
  // 往后台传动系
  async create(params) {
    const cfg = {
      ...params
    };
    const result = await this.app.mysql.insert('commodity', cfg);
    return {
      code: 200,
      msg: '创建成功',
      data: result,
    };
  }
  async delete(uid) {
    const result = await this.app.mysql.delete('commodity', { id: uid });
    return {
      code: 200,
      msg: '删除成功',
      data: result,
    };
  }
  async update(uid, params) {
    const cfg = {
      id: uid,
      ...params,
    };
    const result = await this.app.mysql.update('commodity', cfg);
    return {
      code: 200,
      msg: '更新成功',
      data: result,
    };
  }
  async findAll0(pass) {
    const result = await this.app.mysql.select('commodity', {
      where: { pass },
    });
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }
}

module.exports = commodity;
