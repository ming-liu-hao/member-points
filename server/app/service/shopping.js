'use strict';

const Service = require('egg').Service;

class shopping extends Service { // shopping改成跟后台数据库的名字一致
  async findAll() {
    const result = await this.app.mysql.select('shopping');
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }// timesocietyitem
  async find(uid) {
    const result = await this.app.mysql.get('shopping', { id: uid });
    if (result !== null) {
      return {
        code: 200,
        msg: '查找成功',
        data: result,
      };
    }
    return {
      msg: '没有此条数据！',
    };

  }
  // 往后台传动系
  async create(params) {
    const cfg = {
      ...params
    };
    const result = await this.app.mysql.insert('shopping', cfg);
    return {
      code: 200,
      msg: '创建成功',
      data: result,
    };
  }
  async delete(uid) {
    const result = await this.app.mysql.delete('shopping', { id: uid });
    return {
      code: 200,
      msg: '删除成功',
      data: result,
    };
  }
  async update(uid, params) {
    const cfg = {
      id: uid,
      ...params,
    };
    const result = await this.app.mysql.update('shopping', cfg);
    return {
      code: 200,
      msg: '更新成功',
      data: result,
    };
  }
  async findAll0(pass) {
    const result = await this.app.mysql.select('shopping', {
      where: { pass },
    });
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }
}

module.exports = shopping;
