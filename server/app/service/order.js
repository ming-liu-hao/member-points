'use strict';

const Service = require('egg').Service;

class order extends Service { // order改成跟后台数据库的名字一致
  async findAll() {
    const result = await this.app.mysql.select('order');
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }// timesocietyitem
  async find(uid) {
    const result = await this.app.mysql.get('order', { id: uid });
    if (result !== null) {
      return {
        code: 200,
        msg: '查找成功',
        data: result,
      };
    }
    return {
      msg: '没有此条数据！',
    };

  }
  // 往后台传动系
  async create(params) {
    const cfg = {
      ...params
    };
    const result = await this.app.mysql.insert('order', cfg);
    return {
      code: 200,
      msg: '创建成功',
      data: result,
    };
  }
  async delete(uid) {
    const result = await this.app.mysql.delete('order', { id: uid });
    return {
      code: 200,
      msg: '删除成功',
      data: result,
    };
  }
  async update(uid, params) {
    const cfg = {
      id: uid,
      ...params,
    };
    const result = await this.app.mysql.update('order', cfg);
    return {
      code: 200,
      msg: '更新成功',
      data: result,
    };
  }
  async findAll0(pass) {
    const result = await this.app.mysql.select('order', {
      where: { pass },
    });
    return {
      code: 200,
      msg: '查找成功',
      data: result,
    };
  }
}

module.exports = order;
