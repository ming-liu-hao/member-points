import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    hmr: true,
    //vue3 vite配置热更新不用手动刷新
  }
})
// module.exports = {
//   pluginOptions: {
//     'style-resources-loader': {
//       preProcessor: 'sass',
//       patterns: []
//     }
//   }
// }