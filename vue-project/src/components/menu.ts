import { RouterLink } from "vue-router";
import { h } from "vue";
import { NIcon } from "naive-ui";
import type { MenuOption } from "naive-ui";
import {
    BookOutline as BookIcon,
    PersonOutline as PersonIcon,
    WineOutline as WineIcon,
    HomeOutline as HomeIcon,
} from "@vicons/ionicons5";


function renderIcon(icon: any) {
    return () => h(NIcon, null, { default: () => h(icon) });
}


export const menuOptions: MenuOption[] = [
    {
        label: "规则设置",
        key: "1",
        icon: renderIcon(HomeIcon),
        children: [
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "rule",
                            },
                        },
                        { default: () => "规则设置" }
                    ),
                key: "1-1",
            },
        ],
    },
    {
        label: "积分获取",
        key: "2",
        icon: renderIcon(WineIcon),
        children: [
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "activityintegral",
                            },
                        },
                        { default: () => "活动积分" }
                    ),
                key: "2-1",
            },
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "promotionintegral",
                            },
                        },
                        { default: () => "促销积分" }
                    ),
                key: "2-2",
            },
        ],
    },
    {
        label: "积分消费",
        key: "3",
        icon: renderIcon(PersonIcon),
        children: [
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "category",
                            },
                        },
                        { default: () => "品类管理" }
                    ),
                key: "3-1",
            },
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "commodity",
                            },
                        },
                        { default: () => "商品管理" }
                    ),
                key: "3-2",
            },
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "shoppingmall",
                            },
                        },
                        { default: () => "商城管理" }
                    ),
                key: "3-3",
            },
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "OrderManagement",
                            },
                        },
                        { default: () => "订单管理" }
                    ),
                key: "3-4",
            },
        ],
    },
    {
        label: "积分数据",
        key: "4",
        icon: renderIcon(BookIcon),
        children: [
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "integralgain",
                            },
                        },
                        { default: () => "积分获取" }
                    ),
                key: "4-1",
            },
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "integralalteration",
                            },
                        },
                        { default: () => "积分变更" }
                    ),
                key: "4-2",
            },
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "integralbalance",
                            },
                        },
                        { default: () => "积分平衡" }
                    ),
                key: "4-3",
            },
            {
                label: () =>
                    h(
                        RouterLink,
                        {
                            to: {
                                name: "memberparticulars",
                            },
                        },
                        { default: () => "会员积分明细" }
                    ),
                key: "3-4",
            },
        ],
    },
];