import service from "./request";
// 登录
export function getLogin(){
    return service({
        url: '/login',
        method: 'get'
    })
}
// 获取品类管理
export function getClassguanli(){
    return service({
        url: '/classguanli',
        method: 'get'
    })
}
// 修改品类管理
export function ClassguanliCheckAll(data:any){
    return service({
        url: `/classguanli/put/${data.id}`,
        method: 'PUT',
        data
    })
}
// 删除品类管理
export function ClassguanliDel(data:number){
    return service({
        url: `/classguanli/del/${data}`,
        method: 'DELETE',
    })
}
// 添加品类管理
export function ClassguanliAdd(data:any){
    return service({
        url: `/classguanli/add`,
        method: 'POST',
        data
    })
}
// 上传图片
export function postUpload(url:string, fileList:any) {
    var param = new FormData()
    param.append('file', fileList.raw)

    return service.post(`http://127.0.0.1:7001${url}`, param)
}
// 获取商品管理
export function getCommodity(){
    return service({
        url: `/commodity`,
        method: 'GET',
    })
}
// 添加商品管理
export function CommodityAdd(data:any){
    return service({
        url: `/commodity/add`,
        method: 'POST',
        data
    })
}
// 删除商品管理
export function CommodityDel(data:number){
    return service({
        url: `/commodity/del/${data}`,
        method: 'DELETE',
    })
}
// 修改商品管理
export function CommodityEdit(data:any){
    return service({
        url: `/commodity/put/${data.id}`,
        method: 'PUT',
        data
    })
}

// 获取商城管理
export function getShopping(){
    return service({
        url: `/shopping`,
        method: 'GET',
    })
}
// 添加商城管理
export function ShoppingAdd(data:any){
    return service({
        url: `/shopping/add`,
        method: 'POST',
        data
    })
}
// 删除商城管理
export function ShoppingDel(data:number){
    return service({
        url: `/shopping/del/${data}`,
        method: 'DELETE',
    })
}
// 修改商城管理
export function ShoppingEdit(data:any){
    return service({
        url: `/shopping/put/${data.id}`,
        method: 'PUT',
        data
    })
}

// 获取订单管理
export function getOrder(){
    return service({
        url: `/order`,
        method: 'GET',
    })
}
// 添加订单管理
export function OrderAdd(data:any){
    return service({
        url: `/order/add`,
        method: 'POST',
        data
    })
}
// 删除订单管理
export function OrderDel(data:number){
    return service({
        url: `/order/del/${data}`,
        method: 'DELETE',
    })
}
// 修改订单管理
export function OrderEdit(data:any){
    return service({
        url: `/order/put/${data.id}`,
        method: 'PUT',
        data
    })
}
export function getJfhq(){
    return service({
        url: '/jfhq',
        method: 'get'
    })
}
export function getJfbg(){
    return service({
        url: '/lgjfbg',
        method: 'get'
    })
}
export function getJfph(){
    return service({
        url: '/lgjfph',
        method: 'get'
    })
}
export function getJfmx(){
    return service({
        url: '/lgjfmx',
        method: 'get'
    })
}
export function getActiveList(){
    return service({
        url: '/activelist',
        method: 'get'
    })
}
// 删除
export function DelActiveList(id:number){
    return service({
        url: '/activelist/'+id,
        method: 'delete'
    })
}
// 增加
export function ADDActiveList(data:any){
    return service({
        url: '/activelist',
        method: 'post',
        data
    })
}
// 更新
export function UpdateActiveList(data:any){
    return service({
        url: '/activelist/'+data.id,
        method: 'put',
        data
    })
}