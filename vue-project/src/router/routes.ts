export const routes = [
  {
    path: '/',
    redirect: '/rule'
  },
  {
    // 规则设置
    path: '/rule',
    name: 'rule',
    meta:{title:'规则设置',name:'规则设置'},
    component: () => import('../views/Rule.vue')
  },
  {
    // 活动积分
    path: '/activityintegral',
    name: 'activityintegral',
    meta:{title:'积分获取',name:'活动积分'},
    component: () => import('../views/ActivityIntegral.vue')
  },
  {
    // 促销积分
    path: '/promotionintegral',
    name: 'promotionintegral',
    meta:{title:'积分获取',name:'促销积分'},
    component: () => import('../views/PromotionIntegral.vue')
  },
  {
    // 品类管理
    path: '/category',
    name: 'category',
    meta:{title:'积分消费',name:'品类管理'},
    component: () => import('../views/Category.vue'),
    children:[
      {
        path:'tianjia',
        name:'tianjia',
        meta:{name:'添加品类'},
        component:()=> import('../views/CategoryAdd.vue'),
      }
    ]
  },
  {
    // 商品管理
    path: '/commodity',
    name: 'commodity',
    meta:{title:'积分消费',name:'商品管理'},
    component: () => import('../views/Commodity.vue'),
    children:[
      {
        path:'addsho',
        name:'Addsho',
        meta:{name:'添加商品'},
        component:()=> import('../views/CommodityAdd.vue'),
      }
    ]
  },
  {
    // 商城管理
    path: '/shoppingmall',
    name: 'shoppingmall',
    meta:{title:'积分消费',name:'商城管理'},
    component: () => import('../views/ShoppingMall.vue'),
    children:[
      {
        path:'add',
        name:'Add',
        meta:{name:'添加商品'},
        component:()=> import('../views/ShoppingMallAdd.vue'),
      }
    ]
  },
  {
    // 订单管理
    path: '/OrderManagement',
    name: 'OrderManagement',
    meta:{title:'积分消费',name:'订单管理'},
    component: () => import('../views/OrderManagement.vue'),
    children:[
      {
        path:'add',
        name:'add',
        meta:{name:'添加商品'},
        component:()=> import('../views/OrderManagementAdd.vue'),
      }
    ]
  },
  {
    // 积分获取
    path: '/integralgain',
    name: 'integralgain',
    meta:{title:'积分数据',name:'积分获取'},
    component: () => import('../views/IntegralGain.vue')
  },
  {
    // 积分变更
    path: '/integralalteration',
    name: 'integralalteration',
    meta:{title:'积分数据',name:'积分变更'},
    component: () => import('../views/IntegralAlteration.vue')
  },
  {
    // 积分平衡
    path: '/integralbalance',
    name: 'integralbalance',
    meta:{title:'积分数据',name:'积分平衡'},
    component: () => import('../views/IntegralBalance.vue')
  },
  {
    // 会员积分明细
    path: '/memberparticulars',
    name: 'memberparticulars',
    meta:{title:'积分数据',name:'会员积分明细'},
    component: () => import('../views/MemberParticulars.vue')
  },
  {
    // 登录页
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue')
  }
]